package com.spotify;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage extends BasePage {

    @FindBy(css = "button[data-testid='login-button']")
    private WebElement loginButton;

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public void clickLoginButton() {
        waitForElementToBeClickable(loginButton);
        loginButton.click();
    }
}
