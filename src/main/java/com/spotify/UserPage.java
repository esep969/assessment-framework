package com.spotify;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class UserPage extends BasePage {

    @FindBy(css = "div[data-testid='placeholder-wrapper']")
    private WebElement userIcon;

    @FindBy(css = "span[id='hover-or-focus-tooltip']")
    private WebElement userNameElement;

    public UserPage(WebDriver driver) {
        super(driver);
    }

    public String getUserNameValue() {
        waitForElementBeVisible(userIcon);
        hover(userIcon);
        waitForElementBeVisible(userNameElement);
        return userNameElement.getText();
    }
}
