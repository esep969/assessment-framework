package com.spotify;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public abstract class BasePage {

    public static final int DEFAULT_TIMEOUT = 10;
    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void waitForElementToBeClickable(WebElement element) {
        waitForElementToBeClickable(element, DEFAULT_TIMEOUT);
    }

    public void waitForElementToBeClickable(WebElement element, long timeout) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.of(timeout, ChronoUnit.SECONDS));
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitForElementBeVisible(WebElement element) {
        waitForElementBeVisible(element, DEFAULT_TIMEOUT);
    }

    public void waitForElementBeVisible(WebElement element, long timeToWait) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.of(timeToWait, ChronoUnit.SECONDS));
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void clear(WebElement element) {
        Actions actions = new Actions(driver);
        waitForElementToBeClickable(element, DEFAULT_TIMEOUT);
        element.click();
        actions.keyDown(Keys.CONTROL)
                .sendKeys(Keys.chord("A"))
                .keyUp(Keys.CONTROL)
                .perform();
        element.sendKeys(Keys.BACK_SPACE);
    }

    public void hover(WebElement element) {
        Actions actions = new Actions(driver);
        actions.moveToElement(element).perform();
    }
}
