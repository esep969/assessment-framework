package com.spotify;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {

    @FindBy(id = "login-username")
    private WebElement userNameInput;

    @FindBy(id = "login-password")
    private WebElement passwordInput;

    @FindBy(id = "login-button")
    private WebElement loginButton;

    @FindBy(css = "div[data-testid='username-error'] p")
    private WebElement userNameErrorMessage;

    @FindBy(css = "div[data-testid='password-error'] span")
    private WebElement passwordErrorMessage;

    @FindBy(css = "div[data-encore-id='banner']")
    private WebElement loginErrorMessage;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void inputUserName(String userName) {
        waitForElementToBeClickable(userNameInput);
        clear(userNameInput);
        userNameInput.sendKeys(userName);
    }

    public void inputPassword(String password) {
        waitForElementToBeClickable(passwordInput);
        clear(passwordInput);
        passwordInput.sendKeys(password);
    }

    public void clearUserName() {
        waitForElementToBeClickable(userNameInput);
        clear(userNameInput);
    }

    public void clearPassword() {
        waitForElementToBeClickable(passwordInput);
        clear(passwordInput);
    }

    public void clickLoginButton() {
        waitForElementToBeClickable(loginButton);
        loginButton.click();
    }

    public boolean isLoginErrorMessageDisplayed() {
        waitForElementBeVisible(loginErrorMessage);
        return loginErrorMessage.isDisplayed();
    }

    public String getUserNameErrorMessageValue() {
        waitForElementBeVisible(userNameErrorMessage);
        return userNameErrorMessage.getText();
    }

    public String getPasswordErrorMessageValue() {
        waitForElementBeVisible(passwordErrorMessage);
        return passwordErrorMessage.getText();
    }
    public String getInvalidCredentialsErrorMessageValue() {
        waitForElementBeVisible(loginErrorMessage);
        return loginErrorMessage.getText();
    }
}
