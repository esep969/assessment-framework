package com.spotify;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OpenSpotifyTest extends BaseTest {

    @ParameterizedTest
    @CsvFileSource(resources = "/valid_credentials.csv")
    @DisplayName("[+] Check login with valid user name and password")
    public void testLoginWithValidCredentials(String userName, String password, String name) {
        mainPage.clickLoginButton();
        loginPage.inputUserName(userName);
        loginPage.inputPassword(password);
        loginPage.clickLoginButton();
        assertEquals(name, userPage.getUserNameValue());
    }

    @Test
    @DisplayName("[-] Check empty user name and password")
    public void testEmptyUserNameAndPassword() {
        mainPage.clickLoginButton();
        loginPage.inputUserName(UUID.randomUUID().toString());
        loginPage.inputPassword(UUID.randomUUID().toString());
        loginPage.clearUserName();
        loginPage.clearPassword();
        assertEquals("Please enter your Spotify username or email address.", loginPage.getUserNameErrorMessageValue());
        assertEquals("Please enter your password.", loginPage.getPasswordErrorMessageValue());
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/invalid_credentials.csv")
    @DisplayName("[-] Check login with incorrect user name and password")
    public void testLoginWithIncorrectCredentials(String userName, String password) {
        mainPage.clickLoginButton();
        loginPage.inputUserName(userName);
        loginPage.inputPassword(password);
        loginPage.clickLoginButton();
        assertEquals("Incorrect username or password.", loginPage.getInvalidCredentialsErrorMessageValue());
    }
}
