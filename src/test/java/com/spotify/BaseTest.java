package com.spotify;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public abstract class BaseTest {

    private WebDriver driver;
    protected MainPage mainPage;
    protected LoginPage loginPage;
    protected UserPage userPage;

    @BeforeAll
    public static void beforeAll() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    public void setUp() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.of(5, ChronoUnit.SECONDS));
        mainPage = new MainPage(driver);
        loginPage = new LoginPage(driver);
        userPage = new UserPage(driver);
        driver.get("https://open.spotify.com/");
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

}
